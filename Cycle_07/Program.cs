﻿using System;
//Ввести число с клавиатуры, найти его факториал. 

namespace Cycle_07
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите число, факториал которого необходимо вычислить:");
            int fact = int.Parse(Console.ReadLine());
            int x = 1;

            for (int i = 1; fact >= i; i++)
            {
                x = x * i;
            }
            Console.WriteLine("Факториал числа {0} равен {1}", fact, x);
            Console.ReadKey();
        }
    }
}
