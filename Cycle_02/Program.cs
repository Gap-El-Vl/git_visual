﻿using System;
//Напишите программу, которая выводит на консоль таблицу умножения

namespace Cycle_02
{
    class Program
    {
        static void Main()
        {
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    Console.Write($"{i}*{j}={i * j}\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}